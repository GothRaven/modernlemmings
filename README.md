# README #

This game is a clone version of the original game called Lemmings

### What is this repository for? ###

* the game is a 2D simple version of the original game.
* Version 2.1
* [GAME REPO](https://bitbucket.org/GothRaven/modernlemmings)

### How do I get set up? ###

* To play the game using the executable :
	- java -jar lemmings.jar [levelname]
* To play the game using eclipse :
	- File -> Open Project File From System -> Select the game folder -> go to the main class in the package "lemmings" 
	-> change the game level name -> RUN
* You have the levels in their folder in case you want to modify or create your own levels

### Contribution guidelines ###

* Code review : 29/10/2017 22:09

### Who do I talk to? ###

* Admin : Youssouf Oualhadj
* Team : - Safiy Errahmane ZAGHBANE, Nouredine AMRANE.