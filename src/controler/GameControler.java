package controler;


import exceptions.InvalideFileException;
import modele.Game;
import view.GameFrame;

public class GameControler {
	
	private Game game;
	private GameFrame window;
	
	public GameControler(String fname) throws InvalideFileException 
	{
		game = new Game(fname);
		window = new GameFrame(game);
		game.getLevel().setGameView(window.getGameView());
		window.showHelp();
		while (game.isOn())
		{
			run();
		}
		this.window.end();
	}
	
	public void run() {
		game.update();
		window.show();
	}
}