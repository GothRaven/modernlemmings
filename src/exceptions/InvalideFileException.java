package exceptions;

public class InvalideFileException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalideFileException(String message) {
		super(message);
	}
}
