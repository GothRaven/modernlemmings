package exceptions;

public class TileAlreadyExistsException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TileAlreadyExistsException(String message) {
		super(message);
	}
}
