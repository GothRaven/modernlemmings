package lemmings;

import controler.GameControler;
import exceptions.InvalideFileException;

public class App {

	public static void main(String[] args) {
		if (args.length == 1) {
			try {
				new GameControler(args[0]+".lvl");
			} catch (InvalideFileException e) {
				System.out.println("your chosen file doesn't exist,\n"
						+ "make sure not to write the [.lvl] at the end");
			}
		}else
			System.out.println("java -jar lemmings.jar [levelName]\n"
					+ "you can find the level names in the resources [res] folder\n"
					+ "P.S you can only play one level each time");
	}
}