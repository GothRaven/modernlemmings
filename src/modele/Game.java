package modele;

import java.io.File;

import exceptions.InvalideFileException;
import modele.game.level.Level;

public class Game {
	
	private boolean on;
	private Level level;
	
	public Game(String fname) throws InvalideFileException 
	{	
		String path = System.getProperty("user.dir");
		File fd = new File(path +"/res/levels/"+fname);
		this.level = new Level(this, fd);
		this.on = true;
	}
	
	public void update() {
		level.update();
	}
	
	public boolean isOn() {
		return this.on;
	}
	
	public void end() {
		this.on = false; 
	}
	
	public Level getLevel() {
		return level;
	}
}
