package modele.game.level;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.Timer;

import exceptions.InvalideFileException;
import modele.Game;
import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.info.LevelInfo;
import modele.game.level.lemming.Lemming;
import modele.game.level.lemming.Walker;
import modele.game.level.lemming.powerups.PowerType;
import modele.game.level.map.MapTiles;
import modele.game.level.settings.Settings;
import view.GamePanel;

public class Level {
	
	public static int WIDTH, HEIGHT;
	private LevelInfo info;
	private MapTiles map;
	private ArrayList<Lemming> lemmings;
	private Game game;
	private GamePanel gameView;
	private Timer time;
	private int lemShowSpeedCt, gameSpeedCt, lemShowCt;
	private int selectedKey;
	
	public Level(Game game, File fd) throws InvalideFileException 
	{
		this.game = game;
		this.selectedKey = KeyEvent.VK_A;
		this.map = new MapTiles(fd);
		this.info = new LevelInfo(fd);
		Level.WIDTH = this.map.getMaxAxes().getX();
		Level.HEIGHT = this.map.getMaxAxes().getY();
		this.lemmings = new ArrayList<Lemming>();
		this.time = new Timer(LevelInfo.SPEED_SCALE, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LevelInfo info = Level.this.info;
				if (info.getGameTime() > 0) {
					info.setGameTime(info.getGameTime() - 1);
					Timer time = (Timer) e.getSource();
					time.restart();
				} else {
					Level.this.info.setWon(false);
					Level.this.game.end();
				}
			}
		});
		this.lemShowSpeedCt = 0;
		this.gameSpeedCt = 0;
		this.lemShowCt = 0;
	}
	
	public void update() {
		if (this.info.isEnPause()) {
			
			if (this.time.isRunning()) {
				this.time.stop();
				LevelEvent event = new LevelEvent(lemmings, map, Settings.selectedPower(selectedKey), info);
				gameView.update(event);
			}
		} else {
			if (!this.time.isRunning()) {
				this.time.start();
			}
			if (this.gameSpeedCt < (LevelInfo.SPEED_SCALE / this.info.getGameSpeed())) {
				this.gameSpeedCt++;
			} else {
				if (this.lemShowCt < this.info.getNbLemTotal()) {
					if (this.lemShowSpeedCt < (LevelInfo.SPEED_SCALE / this.info.getLimShowSpeed())) {
						this.lemShowSpeedCt++;
					} else {
						Position p = this.map.getEnterPos();
						Lemming walker = new Walker(new Position(p), Direction.RIGHT);
						this.lemmings.add(walker);
						
						this.info.setNbLemInGame(this.info.getNbLemInGame() + 1);
						this.lemShowCt++;
						this.lemShowSpeedCt = 0;
					}
				}
				for (Iterator<Lemming> it = this.lemmings.iterator(); it.hasNext();) {
					Lemming lemming = (Lemming) it.next();
					if (lemming.isDead()) {
						this.info.setNbLemDead(this.info.getNbLemDead() + 1);
						this.info.setNbLemInGame(this.info.getNbLemInGame() - 1 );
						it.remove();
					} else if (lemming.isSaved()) {
						this.info.setNbLemSaved(this.info.getNbLemSaved() + 1);
						this.info.setNbLemInGame(this.info.getNbLemInGame() - 1);
						it.remove();
					} else if (lemming.inGame()) {
						lemming.update(map, lemmings);
					}
				}
				
				if (this.lemShowCt == this.info.getNbLemTotal() & this.info.getNbLemInGame() == 0) {
					if (info.getNbLemSaved() >= info.getNbLemToSave())
						this.info.setWon(true);
					else
						this.info.setWon(false);
					this.game.end();
				}
				
				this.gameSpeedCt = 0;
				LevelEvent event = new LevelEvent(lemmings, map, Settings.selectedPower(selectedKey), info);
				gameView.update(event);
			}
		}
	}
	
	public void keyPressed(int keyCode) {
		if (keyCode == KeyEvent.VK_P)
			if (info.isEnPause() == true)
				info.setEnPause(false);
			else 
				info.setEnPause(true);
		if (Settings.isPowerUpKey(keyCode))
			selectedKey = keyCode;
	}

	public void mouseClicked(Position position) {
		for (Iterator<Lemming> it = lemmings.iterator(); it.hasNext();) {
			Lemming l = (Lemming) it.next();
			if (l.getPosition().equals(position)) {
				PowerType power = Settings.selectedPower(selectedKey);
				if (Settings.canPowerUp(l, power)) {
					Lemming lemming = Settings.PowerUp(l, power);
					it.remove();
					this.lemmings.add(lemming);
					break;
				}
			}
		}
	}
	
	public LevelInfo getInfo() {
		return info;
	}
	
	public void setGameView(GamePanel gameView) {
		this.gameView = gameView;
	}
	
}
