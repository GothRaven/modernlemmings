package modele.game.level;

import java.util.ArrayList;

import modele.game.level.info.LevelInfo;
import modele.game.level.lemming.Lemming;
import modele.game.level.lemming.powerups.PowerType;
import modele.game.level.map.MapTiles;

public class LevelEvent {
	
	private ArrayList<Lemming> lemmings;
	private MapTiles map;
	private PowerType powerSelected;
	private LevelInfo info;
	
	public LevelEvent(ArrayList<Lemming> lemmings, MapTiles map, PowerType powerSelected, LevelInfo info) {
		this.lemmings = lemmings;
		this.map = map;
		this.info = info;
		this.powerSelected = powerSelected;
	}
	
	public ArrayList<Lemming> getLemmings() {
		return lemmings;
	}
	
	public LevelInfo getInfo() {
		return info;
	}
	
	public MapTiles getMap() {
		return map;
	}
	
	public PowerType getPowerSelected() {
		return powerSelected;
	}
}
