package modele.game.level.geometry;


public enum Direction {
	RIGHT(1, 0), LEFT(-1, 0), UP(0, -1), DOWN(0, 1), UPPERLEFT(-1, -1), DOWNERLEFT(-1, 1), UPPERRIGHT(1, -1), DOWNERRIGHT(1, 1);
	
	private int x;
	private int y;
	
	Direction(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	Direction(Direction dir) {
		this.x = dir.getX();
		this.y = dir.getY();
	}
	
	public boolean equals(Direction dir) {
		if (x == dir.getX() & y == dir.getY())
			return true;
		else
			return false;
	}
	
	public Direction upper() {
		if (this.equals(Direction.RIGHT))
			return Direction.UPPERRIGHT;
		if (this.equals(Direction.LEFT))
			return Direction.UPPERLEFT;
		else 
			return null;
	}
	
	public Direction downer() {
		if (this.equals(RIGHT) == true)
			return Direction.DOWNERRIGHT;
		if (this.equals(LEFT) == true)
			return Direction.DOWNERLEFT;
		else 
			return null;
	}
	
	public Direction copy() {
		if (this.equals(RIGHT))
			return Direction.RIGHT;
		else if (this.equals(LEFT))
			return Direction.LEFT;
		else if (this.equals(UP))
			return Direction.UP;
		else if (this.equals(DOWN))
			return Direction.DOWN;
		else if (this.equals(UPPERLEFT))
			return Direction.UPPERLEFT;
		else if (this.equals(DOWNERLEFT))
			return Direction.DOWNERLEFT;
		else if (this.equals(UPPERRIGHT))
			return Direction.UPPERRIGHT;
		else
			return Direction.DOWNERRIGHT;
	}
	
	public Direction oppositDirection(Direction dir) {
		if (dir.equals(RIGHT))
			return Direction.LEFT;
		else if (dir.equals(LEFT))
			return Direction.RIGHT;
		else if (dir.equals(UP))
			return Direction.DOWN;
		else if (dir.equals(DOWN))
			return Direction.UP;
		else if (dir.equals(UPPERLEFT))
			return Direction.DOWNERRIGHT;
		else if (dir.equals(DOWNERLEFT))
			return Direction.UPPERRIGHT;
		else if (dir.equals(UPPERRIGHT))
			return Direction.DOWNERLEFT;
		else
			return Direction.UPPERLEFT;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
}

	
