package modele.game.level.geometry;


public class Position {
	private int x, y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Position(Position p) {
		this.x = p.getX();
		this.y = p.getY();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Position)
			return equals(obj);
		return false;
	}
	
	public boolean equals(Position p) {
		return ((this.x == p.getX()) && (this.y == p.getY()));
	}
	
	public Position translate(Position src, Position dest) {
		return (new Position(src.getX() + dest.getX(), src.getY() + dest.getY()));
	}
	
	public Position nextPosition(Direction d)
	{
		return (new Position(x + d.getX(), y + d.getY()));
	}
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	@Override
	public String toString() {
		return " x= " + this.x + ", y = " + this.y;
	}
}
