package modele.game.level.lemming;

import java.awt.Color;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;

public class Blocker extends Lemming {

	
	public Blocker(Position p, Direction d) {
		super(p, d);
		this.color = Color.black;
	}
	
	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		
		
		
		if (shouldFall(map)) {
			this.fall();
			fallingCounter++;
		}
		
		super.update(map, lemmings);
		
		if (fallingCounter >= MAX_HEIGHT)
			this.kill();
	}
	
	@Override
	public String toString() {
		return "Blocker , x = "+position.getX()+", y= "+position.getY();
	}

}
