package modele.game.level.lemming;

import java.awt.Color;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;

public class Bomber extends Lemming {
	private int counter = 0;
	
	public Bomber(Position p, Direction d) {
		super(p, d);
		this.color = Color.RED.darker();
	}
	
	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		if(nextIsBlocker(lemmings) )
			this.direction = direction.oppositDirection(direction);
		
		if (shouldFall(map)&!lDownIsBlooker(lemmings)) {
			this.fall();
			fallingCounter++;
		} else {
			if (shouldMove(map)) {
				this.move();
			
			} else {
				if (shouldJump(map))
					this.jump();	
				else
					this.direction = direction.oppositDirection(direction);
			}
			fallingCounter = 0;
		}
		
		super.update(map, lemmings);
		
		
		if (fallingCounter >= MAX_HEIGHT)
			this.kill();
		

	
		this.counter++;
		if (counter == 4) {
			this.explode(map, lemmings);
			this.kill();
		}
	}
	
	
	
	@Override
	public String toString() {
		return "Bomber , x = "+position.getX()+", y= "+position.getY();
	}

}
