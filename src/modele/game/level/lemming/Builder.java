package modele.game.level.lemming;

import java.awt.Color;
import java.util.ArrayList;

import exceptions.TileAlreadyExistsException;
import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;
import modele.game.level.map.Tile;
import modele.game.level.map.TileType;

public class Builder extends Lemming {
	
	
	private int Step;
	private boolean isBuilding;
	public Builder(Position p, Direction d) {
		super(p, d);
		Step = 0;
		this.color = Color.YELLOW.darker();
		isBuilding= true;
	}
	
	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		if(nextIsBlocker(lemmings))
			this.direction = direction.oppositDirection(direction);
		
		Tile tDUpper = map.getTileInThisDirection(position, direction.upper());
		Tile tDirection = map.getTileInThisDirection(position, direction);
		if(isBuilding & Step <= 3) {
			if(tDirection == null) {
				if (tDUpper == null) {
					Step++;
					this.build(map);
					 if(shouldJump(map) )
						this.jump();
				} else {
					this.build(map);
					isBuilding= false;
					this.direction = direction.oppositDirection(direction);
				}
			} else {
				isBuilding=false;
			}
		} else {
			if (shouldFall(map) &!lDownIsBlooker(lemmings)) {
				this.fall();
				fallingCounter++;
			} else {
				if (shouldMove(map)) {
					this.move();
				} else {
					if (shouldJump(map) & !lDUppreIsBlooker(lemmings))
						this.jump();	
					else
						this.direction = direction.oppositDirection(direction);
				}
				fallingCounter = 0;
			}
		}
		
		super.update(map, lemmings);
		if (fallingCounter >= MAX_HEIGHT)
			this.kill();
	}
	
	private void build(MapTiles map) {
		try {
			map.addTile(new Position(position.getX() + direction.getX(), position.getY() + direction.getY()), TileType.BOX);
		} catch (TileAlreadyExistsException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Builder , x = "+position.getX()+", y= "+position.getY();
	}

}
