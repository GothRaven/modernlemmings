package modele.game.level.lemming;

import java.awt.Color;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;
import modele.game.level.map.Tile;
import modele.game.level.map.TileType;


public class Climber extends Walker {
	
	private boolean isClimbing;
	
	public Climber(Position p, Direction d) {
		super(p, d);
		this.isClimbing = false;
		this.color = Color.GREEN.brighter();
	}

	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		if(nextIsBlocker(lemmings))
			this.direction = direction.oppositDirection(direction);

		if ( shouldFall(map)& !isClimbing &!lDownIsBlooker(lemmings)) {
			this.fall();
			fallingCounter++;
		} else {	
			if (shouldMove(map)) {
				this.move();
				isClimbing=false;
			} else {
				Tile tUp = map.getTileInThisDirection(position, Direction.UP);
				if (tUp == null && !lDUppreIsBlooker(lemmings)||tUp.getType() == TileType.EXIT && !lDUppreIsBlooker(lemmings))
					climbe();	
				else {
					this.direction = direction.oppositDirection(direction);
					isClimbing=false;
				}
					
			}
			fallingCounter = 0;
		}
		super.update(map, lemmings);
		if (fallingCounter >= MAX_HEIGHT)
			this.kill();
	}
	
	
	private void climbe() {
		isClimbing= true;
		this.position.setX(position.getX() + Direction.UP.getX());
		this.position.setY(position.getY() + Direction.UP.getY());
	}
	
/*	@Override
	public void update(MapTiles map) {
		
			
		
		if(getTypeTileInThisDirection(map,Direction.DOWN) == null & this.jump== false || getTypeTileInThisDirection(map,Direction.DOWN) == TileType.EXIT || getTypeTileInThisDirection(map,Direction.DOWN) == TileType.LAVA ) {
			position.setX(position.getX() + Direction.DOWN.getX());
			position.setY(position.getY() + Direction.DOWN.getY());
			goingDown++;
		}else {
			if (getTypeTileInThisDirection(map, direction)== null || getTypeTileInThisDirection(map,direction) == TileType.EXIT || getTypeTileInThisDirection(map,Direction.DOWN) == TileType.LAVA) {
				position.setX(position.getX() + direction.getX());
				position.setY(position.getY() + direction.getY());
				this.jump= false;
				
			}else {
				if (getTypeTileInThisDirection(map, Direction.UP) == null 
					||	getTypeTileInThisDirection(map, Direction.UP) == TileType.EXIT 
					||	getTypeTileInThisDirection(map, Direction.UP) == TileType.LAVA) {
					position.setX(position.getX() + Direction.UP.getX());
					position.setY(position.getY() + Direction.UP.getY());
					this.jump= true;
				}else {
					this.jump= false;
					changeToOppositDirection();	
				}
					
			}
			goingDown = 0;	
		}
		
															 			SavedOrDead			
		
		if(whereAmI(map) == TileType.EXIT) {
			Save();
		}else if(whereAmI(map) == TileType.LAVA){
			this.isDead = true;
		}
		if (goingDown >= 5)
			this.isDead = true;		
	}*/
	
	@Override
	public String toString() {
		return "Climber , x = "+position.getX()+", y= "+position.getY();
	}
	
	

}
