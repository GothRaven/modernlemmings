package modele.game.level.lemming;

import java.awt.Color;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;
import modele.game.level.map.Tile;


public class Digger extends Lemming {

	private int Step = 0;
	public Digger(Position p, Direction d) {
		super(p, d);
		this.color = Color.CYAN.darker();
	}

	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		if(nextIsBlocker(lemmings))
			this.direction = direction.oppositDirection(direction);
		
		Tile tDown = map.getTileInThisDirection(position, Direction.DOWN);

		if (shouldFall(map)&!lDownIsBlooker(lemmings)) {
			this.fall();
			fallingCounter++;
		} else {
			if(tDown.getType().isDestructible()& Step<=5 & tDown.getType()!=null) {
				Step++;
				map.removeTile(new Position(position.getX()+Direction.DOWN.getX(),position.getY()+Direction.DOWN.getY()));
				this.fall();
				fallingCounter=0;
			}else {
				if (shouldMove(map)) {
					this.move();
				} else {
					if (shouldJump(map)& !lDUppreIsBlooker(lemmings))
						this.jump();	
					else
						this.direction = direction.oppositDirection(direction);
				}
			}
			
			fallingCounter = 0;
		}
		super.update(map, lemmings);
		if (fallingCounter >= MAX_HEIGHT)
			this.kill();
	}
	
/*	@Override
	public void update(MapTiles map) {
		
	
			if(getTypeTileInThisDirection(map,Direction.DOWN) == null & this.jump== false || getTypeTileInThisDirection(map,Direction.DOWN) == TileType.EXIT || getTypeTileInThisDirection(map,Direction.DOWN) == TileType.LAVA ) {
				position.setX(position.getX() + Direction.DOWN.getX());
				position.setY(position.getY() + Direction.DOWN.getY());
				goingDown++;
			}else {
				if(Step<=5) {
					if(isDestructible(map,new Position( position.getX()+Direction.DOWN.getX(),position.getY()+Direction.DOWN.getY()))) {
						map.removeTile(new Position(position.getX()+Direction.DOWN.getX(),position.getY()+Direction.DOWN.getY()));
						position.setX(position.getX() + Direction.DOWN.getX());
						position.setY(position.getY() + Direction.DOWN.getY());
						goingDown = 0;
					}else {
						Step =6;
					}
				}else {
					if (getTypeTileInThisDirection(map, direction)== null || getTypeTileInThisDirection(map,direction) == TileType.EXIT || getTypeTileInThisDirection(map,Direction.DOWN) == TileType.LAVA) {
						position.setX(position.getX() + direction.getX());
						position.setY(position.getY() + direction.getY());
						this.jump= false;
					}else {
						if (getTypeTileInThisDirection(map, Direction.UP) == null & map.getTile(new Position(position.getX()+direction.getX()+Direction.UP.getX(),position.getY()+direction.getY()+Direction.UP.getY()))== null
							||	getTypeTileInThisDirection(map, Direction.UP) == null & map.getTile(new Position(position.getX()+direction.getX()+Direction.UP.getX(),position.getY()+direction.getY()+Direction.UP.getY())).getType() == TileType.EXIT 
							||	getTypeTileInThisDirection(map, Direction.UP) == null & map.getTile(new Position(position.getX()+direction.getX()+Direction.UP.getX(),position.getY()+direction.getY()+Direction.UP.getY())).getType() == TileType.LAVA) {
							position.setX(position.getX() + Direction.UP.getX());
							position.setY(position.getY() + Direction.UP.getY());
							this.jump= true;
						}else {
							this.jump= false;
							changeToOppositDirection();	
						}
							
					}
					goingDown = 0;	
				}
				
			}
		
		
		
		
															 			SavedOrDead			
		
		if(whereAmI(map) == TileType.EXIT) {
			Save();
		}else if(whereAmI(map) == TileType.LAVA){
			this.isDead = true;
		}
		if (goingDown >= 5)
			this.isDead = true;
		
		
		
		
		
	}*/
	@Override
	public String toString() {
		return "Digger , x = "+position.getX()+", y= "+position.getY();
	}

}
