package modele.game.level.lemming;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;
import modele.game.level.map.Tile;
import modele.game.level.map.TileType;
import view.GamePanel;

public abstract class Lemming {
	
	protected static int MAX_HEIGHT = 5;
	protected boolean isDead;
	protected boolean inGame;
	protected boolean isSaved;
	protected Color color;
	protected Position position;
	protected Direction direction;
	protected int fallingCounter;
	
	public Lemming(Position p, Direction d) {
		this.position = p;
		this.direction = d;
		inGame = true;
		isDead = false;
		isSaved = false;
		fallingCounter = 0;
	}
	
	public  void update(MapTiles map, ArrayList<Lemming> lemmings) {
		
		Tile tMe = map.getTile(position);
		if (tMe != null) {
			if(tMe.getType() == TileType.EXIT) 
				this.save();
			else if(tMe.getType() == TileType.LAVA)
				this.kill();
			else if(tMe.getType() == TileType.TELEPORT)
				this.teleport(map);
			else if (tMe.getType() == TileType.BOMB)
				explode(map, lemmings);
		}

	}
	
	public void draw(Graphics g) {
		g.setColor(color);
		g.fillOval(position.getX() * GamePanel.SCALE, position.getY() * GamePanel.SCALE, 
				GamePanel.SCALE, GamePanel.SCALE);
	}
	
	protected void fall() {
		position.setX(position.getX() + Direction.DOWN.getX());
		position.setY(position.getY() + Direction.DOWN.getY());
	}
	
	protected void move() {
		position.setX(position.getX() + direction.getX());
		position.setY(position.getY() + direction.getY());
	}
	
	protected void jump() {
		Direction dir = direction.upper();
		position.setX(position.getX() + dir.getX());
		position.setY(position.getY() + dir.getY());
	}
	
	protected void teleport(MapTiles map) {
		Position p = map.teleport(position);
		if (p != null)
			this.position = new Position(p);
	}
	
	protected boolean nextIsBlocker(ArrayList<Lemming> lemmings) {
		Position nextPos = new Position(position.getX() + direction.getX(), position.getY() + direction.getY());
		for (Lemming l : lemmings) {
			if (l instanceof Blocker & l.getPosition().equals(nextPos))
				return true;
		}
		return false;
	}
	
	protected boolean lDownIsBlooker(ArrayList<Lemming> lemmings) {
		Position nextPos = new Position(position.getX() +Direction.DOWN.getX(), position.getY() + Direction.DOWN.getY());
		for (Lemming l : lemmings) {
			if (l instanceof Blocker & l.getPosition().equals(nextPos))
				return true;
		}
		return false;
	}
	protected boolean lDUppreIsBlooker(ArrayList<Lemming> lemmings) {
		Position nextPos = new Position(position.getX() + direction.getX()+Direction.UP.getX(), position.getY() + direction.getY()+Direction.UP.getY());
		for (Lemming l : lemmings) {
			if (l instanceof Blocker & l.getPosition().equals(nextPos))
				return true;
		}
		return false;
	}
	protected boolean shouldFall(MapTiles map) {
		Tile tDown = map.getTileInThisDirection(position, Direction.DOWN);
		
		if (tDown == null ||  tDown.getType() == TileType.BOMB || tDown.getType() == TileType.EXIT || tDown.getType() == TileType.LAVA || tDown.getType() == TileType.TELEPORT)
			return true;
		return false;
	}
	
	protected void explode(MapTiles map, ArrayList<Lemming> lemmings) {
		int x = position.getX(), y = position.getY();
		ArrayList<Position> positions = new ArrayList<Position>();
		positions.add(new Position(x - 1, y - 1));
		positions.add(new Position(x, y - 1));
		positions.add(new Position(x + 1, y - 1));
		positions.add(new Position(x - 1, y));
		positions.add(new Position(x + 1, y));
		positions.add(new Position(x - 1, y + 1));
		positions.add(new Position(x, y + 1));
		positions.add(new Position(x + 1, y + 1));
		
		//Remove the distructible map tiles
		for (Position p : positions) {
			Tile t = map.getTile(p);
			if (t != null)
				if (t.getType().isDestructible())
					map.removeTile(p);
		}
		
		map.removeTile(position);
		
		//Kill the lemmings arround
		for (Position p : positions) {
			for (Lemming l : lemmings) {
				if (l.getPosition().equals(p))
					l.kill();
			}
		}
		this.kill();	
	}
	
	protected boolean shouldJump(MapTiles map) {
		Tile tUp = map.getTileInThisDirection(position, Direction.UP);
		Tile tDUpper = map.getTileInThisDirection(position, direction.upper());
		if (tUp == null &(  tDUpper == null || tDUpper.getType() == TileType.BOMB || tDUpper.getType() == TileType.EXIT ||  tDUpper.getType() == TileType.TELEPORT ||tDUpper.getType() == TileType.LAVA))
			return true;
		return false;
	}
	protected boolean shouldMove(MapTiles map) {
		Tile tDirection = map.getTileInThisDirection(position, direction);
		
		if(tDirection == null || tDirection.getType() == TileType.EXIT  || tDirection.getType() == TileType.BOMB || tDirection.getType() == TileType.LAVA || tDirection.getType() == TileType.TELEPORT)
			return true;
		return false;
	}
	
	protected void save() {
		this.isSaved = true;
	}
	
	protected void kill() {
		this.isDead = true;
	}
	
	public boolean isSaved() {
		return this.isSaved;
	}
	
	public boolean isDead() {
		return this.isDead;
	}
	
	public boolean inGame() {
		return this.inGame;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}
	
	
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public Direction getDirection() {
		return direction;
	}

}
