package modele.game.level.lemming;

import java.awt.Color;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;
import modele.game.level.map.Tile;

public class Miner extends Lemming {
	private boolean POWER ,START ;
	
	public Miner(Position p, Direction d) {
		super(p, d);
		this.color = Color.PINK.darker();
		POWER = true;
		START= false;
	}
	
	
	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		if(nextIsBlocker(lemmings))
			this.direction = direction.oppositDirection(direction);
		
		if (shouldFall(map)&!lDownIsBlooker(lemmings)) {
			this.fall();
			fallingCounter++;
		}else {
			Tile tDirection = map.getTileInThisDirection(position, direction);
			if (shouldMove(map)) {
				this.move();
				if(START)
					POWER = false;
			} else {
				if(tDirection.getType().isDestructible() && POWER) {
					map.removeTile(new Position(position.getX()+direction.getX(),position.getY()+direction.getY()));
					this.move();
					START= true;
				}else {
					if (shouldJump(map)& !lDUppreIsBlooker(lemmings))
						this.jump();	
					else
						this.direction = direction.oppositDirection(direction);
				}
				
			}
			fallingCounter = 0;
		}
		
		super.update(map, lemmings);
		if (fallingCounter >= MAX_HEIGHT)
			this.kill();

	}
	

	
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Miner , x = "+position.getX()+", y= "+position.getY();
	}

}
