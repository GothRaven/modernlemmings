package modele.game.level.lemming;
import java.awt.Color;
import java.util.ArrayList;

import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;
import modele.game.level.map.MapTiles;

public class Paratrooper extends Lemming {
	
	private boolean isFlying = false;
	
	public Paratrooper(Position p, Direction d) {
		super(p, d);
		this.color = Color.BLUE;
	}
	
	@Override
	public void update(MapTiles map, ArrayList<Lemming> lemmings) {
		if(nextIsBlocker(lemmings))
			this.direction = direction.oppositDirection(direction);
		
		if (shouldFall(map) &!lDownIsBlooker(lemmings)) {			
			if (!isFlying) {
				this.fall();
				isFlying = true;
			} else
				isFlying = false;
		} else {		
			if (shouldMove(map)) {
				this.move();
			} else {	
				if (shouldJump(map)& !lDUppreIsBlooker(lemmings))
					this.jump();	
				else
					this.direction = direction.oppositDirection(direction);
			}
		}
		super.update(map, lemmings);
	}

	@Override
	public String toString() {
		return "Paratrooper , x = "+position.getX()+", y= "+position.getY();
	}
	

}