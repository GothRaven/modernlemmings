package modele.game.level.lemming.powerups;

public enum PowerType {
	CLIMBER("Climber"), PARATROOPER("Paratrooper"), BLOCKER("Blocker"), 
	BOMBER("Bomber"), BUILDER("Builder"), DIGGER("Digger"), MINER("Miner");

	String name;
	
	PowerType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
