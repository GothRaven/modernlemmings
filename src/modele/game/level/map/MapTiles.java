package modele.game.level.map;

import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import exceptions.InvalideFileException;
import exceptions.TileAlreadyExistsException;
import modele.game.level.geometry.Direction;
import modele.game.level.geometry.Position;

public class MapTiles {

	private HashMap<Position, Tile> map;
	
	private Position enterPos;
	private Position exitPos;

	public MapTiles(File fd) throws InvalideFileException
	{
		try {
			
			InputStream ips = new FileInputStream(fd);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader buff = new BufferedReader(ipsr);
			String line;
			boolean isMap = false;
			int y = 0;
			while ((line = buff.readLine()) != null) {
				if (line.startsWith("map")) {
					isMap = true;
					this.map = new HashMap<Position, Tile>();
					this.enterPos = null;
					this.exitPos = null;
				}else if (isMap) {
					for (int x = 0; x < line.length(); x++) {
						switch (line.charAt(x)) {
							case '+': // BOX
								this.map.put(new Position(x, y), new Tile(TileType.BOX));
								break;
							case '@': // EXIT
								if (this.exitPos != null)
									throw new InvalideFileException("there are more then one exit");
								else
								{
									this.map.put(new Position(x, y), new Tile(TileType.EXIT));
									this.exitPos = new Position(x, y);
								}
								break;
							case 48: // ENTER
								if (this.enterPos != null)
									throw new InvalideFileException("there are more then one entrence");
								else
								{
									this.map.put(new Position(x, y), new Tile(TileType.ENTER));
									this.enterPos = new Position(x, y);
								}
								break;
							case '-': //WALL
								this.map.put(new Position(x, y), new Tile(TileType.WALL));
								break;
							case '#': //LAVA
								this.map.put(new Position(x, y), new Tile(TileType.LAVA));
								break;
							case '%': //TELEPORT_Enter
								this.map.put(new Position(x, y), new Tile(TileType.TELEPORT));
								break;
							case 'B': //BOMB
								this.map.put(new Position(x, y), new Tile(TileType.BOMB));
								break;
							case '&': //MAGIC
								this.map.put(new Position(x, y), new Tile(TileType.MAGIC));
						}
					}
					y++;
				}
			}
			buff.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalideFileException("File is not a map");
		}
	}
	
	public MapTiles(MapTiles maptiles) {
		this.enterPos = new Position(maptiles.getEnterPos());
		this.exitPos = new Position(maptiles.getExitPos());
		this.map = new HashMap<Position, Tile>();
		this.map.putAll(maptiles.getMap());
	}
	
	public Position teleport(Position p) {
		Set<Position> positions = this.map.keySet();
		for (Position position : positions) {
			if (!position.equals(p))
				if (map.get(position).getType() == TileType.TELEPORT)
					return position;
		}
		return null;
	}
	
	public Position getMaxAxes() {
		int xMax = 0;
		int yMax = 0;

		Set<Position> possitions = this.map.keySet();
		for (Position pos : possitions) {

			if (pos.getX() > xMax) {
				xMax = pos.getX();
			}

			if (pos.getY() > yMax) {
				yMax = pos.getY();
			}
		}

		return new Position(xMax + 1, yMax + 1);
	}
	
	
	public void addTile(Position p, TileType type) throws TileAlreadyExistsException 
	{
		if (getTile(p) != null)
			throw new TileAlreadyExistsException("there is already a tile in x="+p.getX()+", y="+p.getY());
		else
			this.map.put(p, new Tile(type));
	}
	
	
	public void removeTile(Position p)
	{
		Set<Position> positions = this.map.keySet();
		Position toRemove = null;
		for (Position position : positions) {
			if (position.equals(p)) {
				toRemove = position;
				break;
			}
		}
		if (toRemove != null)
			this.map.remove(toRemove);
	}
	
	public Tile getTileInThisDirection(Position p, Direction d) {
		
		return getTile(new Position(p.getX()+d.getX(),p.getY()+d.getY()));
		
	}
	
	public Tile getTile(Position p)
	{
		Set<Position> positions = this.map.keySet();
		for (Position position : positions) {
			if (position.equals(p))
				return this.map.get(position); 
		}
		return null;
	}
	
	public void draw(Graphics g) {
		Set<Position> positions = this.map.keySet();
		for (Iterator<Position> it = positions.iterator(); it.hasNext();) {
			Position position = (Position) it.next();
			Tile t = this.map.get(position);
			if (t != null)
				t.draw(g , position);
		}
	}
	
	public Position getEnterPos() {
		return enterPos;
	}
	
	public Position getExitPos() {
		return exitPos;
	}

	public HashMap<Position, Tile> getMap() {
		return map;
	}
	
	public String toString() {
		String result = "";
		Set<Position> positions = this.map.keySet();
		for (Position position : positions) {
			if (position.getY() == 4)
				result += position.toString() + " " + this.map.get(position).toString() +"\n"; 
		}
		return result;
	}

}
