package modele.game.level.map;


import java.awt.Color;
import java.awt.Graphics;

import modele.game.level.geometry.Position;
import view.GamePanel;

public class Tile {
	
	private TileType type;
	private Color color;
	
	public Tile(TileType type) {
		this.type = type;
		this.color = type.getColor();
	}
	
	public void draw(Graphics g, Position p)
	{
		g.setColor(this.color);
		g.fillRect(p.getX() * GamePanel.SCALE, p.getY() * GamePanel.SCALE, 
					GamePanel.SCALE, GamePanel.SCALE);
	}
	
	
	public TileType getType() {
		return type;
	}

}
