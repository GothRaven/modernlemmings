package modele.game.level.map;

import java.awt.Color;

public enum TileType {
	WALL(Color.BLACK, false), BOX(Color.ORANGE,true), LAVA(Color.RED, false),
	ENTER(Color.GRAY, false), EXIT(Color.YELLOW, false), BOMB(Color.PINK, true),
	MAGIC(Color.GREEN, true), TELEPORT(Color.BLUE, false);
	
	private Color color;
	private boolean destructible;
	
	TileType(Color color, boolean destructible) {
		this.color = color;
		this.destructible = destructible;
	}
	
	public Color getColor() {
		return color;
	}
	
	public boolean isDestructible() {
		return destructible;
	}
}
