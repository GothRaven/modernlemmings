package modele.game.level.settings;

import java.awt.event.KeyEvent;

import modele.game.level.geometry.Position;
import modele.game.level.lemming.Blocker;
import modele.game.level.lemming.Bomber;
import modele.game.level.lemming.Builder;
import modele.game.level.lemming.Climber;
import modele.game.level.lemming.Digger;
import modele.game.level.lemming.Lemming;
import modele.game.level.lemming.Miner;
import modele.game.level.lemming.Paratrooper;
import modele.game.level.lemming.powerups.PowerType;

public class Settings {
	
	public static boolean isPowerUpKey(int keyCode) {
		if (keyCode == KeyEvent.VK_A 
				|| keyCode == KeyEvent.VK_Z
				|| keyCode == KeyEvent.VK_E
				|| keyCode == KeyEvent.VK_Q
				|| keyCode == KeyEvent.VK_S
				|| keyCode == KeyEvent.VK_D
				|| keyCode == KeyEvent.VK_W
				)
			return true;
		return false;
	}
	
	public static boolean canPowerUp(Lemming lem, PowerType p) {
		if (lem instanceof Bomber)
			return false;
		if (lem instanceof Blocker && !(p.getName().equals("Bomber")))
			return false;
		return true;
	}

	public static Lemming PowerUp(Lemming lemming, PowerType power) {
		if (power == PowerType.BLOCKER)
			return new Blocker(new Position(lemming.getPosition()), lemming.getDirection().copy());
		if (power == PowerType.BOMBER)
			return new Bomber(new Position(lemming.getPosition()), lemming.getDirection().copy());
		if (power == PowerType.CLIMBER)
			return new Climber(new Position(lemming.getPosition()), lemming.getDirection().copy());
		if (power == PowerType.DIGGER)
			return new Digger(new Position(lemming.getPosition()), lemming.getDirection().copy());
		if (power == PowerType.MINER)
			return new Miner(new Position(lemming.getPosition()), lemming.getDirection().copy());
		if (power == PowerType.PARATROOPER)
			return new Paratrooper(new Position(lemming.getPosition()), lemming.getDirection());
		if (power == PowerType.BUILDER)
			return new Builder(new Position(lemming.getPosition()), lemming.getDirection());
		return null;
	}

	public static PowerType selectedPower(int selectedKey) {
		if (selectedKey == KeyEvent.VK_A)
			return PowerType.BLOCKER;
		if (selectedKey == KeyEvent.VK_Z)
			return PowerType.CLIMBER;
		if (selectedKey == KeyEvent.VK_E)
			return PowerType.BUILDER;
		if (selectedKey == KeyEvent.VK_Q)
			return PowerType.DIGGER;
		if (selectedKey == KeyEvent.VK_S)
			return PowerType.MINER;
		if (selectedKey == KeyEvent.VK_D)
			return PowerType.PARATROOPER;
		if (selectedKey == KeyEvent.VK_W)
			return PowerType.BOMBER;
		return null;
	}
}
